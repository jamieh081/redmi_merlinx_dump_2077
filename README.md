## merlin-user 11 RP1A.200720.011 V12.0.1.0.RJOMIXM release-keys
- Manufacturer: xiaomi
- Platform: mt6768
- Codename: merlinx
- Brand: Redmi
- Flavor: aosp_merlinx-userdebug
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.androi.20211012.074137
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/merlin/merlin:11/RP1A.200720.011/V12.0.1.0.RJOMIXM:user/release-keys
- OTA version: 
- Branch: merlin-user-11-RP1A.200720.011-V12.0.1.0.RJOMIXM-release-keys
- Repo: redmi_merlinx_dump_2077


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
